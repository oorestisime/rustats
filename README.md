[![pipeline status](https://gitlab.com/oorestisime/rustats/badges/master/pipeline.svg)](https://gitlab.com/oorestisime/rustats/commits/master)

# Rustats
This project is a basic CLF log (nginx, apache) parser and analyzer.


## License

Rustats is an open source software under the [GPL v3](https://opensource.org/licenses/gpl-3.0.html)
license, see the [LICENSE](./LICENSE) file in the project root for the full license text.
