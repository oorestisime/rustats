use std::io;
use std::collections::btree_map::BTreeMap;
use std::iter::FromIterator;


use tui::Terminal;
use tui::backend::RawBackend;
use tui::widgets::{BarChart, Block, Item, List, Borders, Widget, Paragraph};
use tui::layout::{Direction, Group, Rect, Size};
use tui::style::{Color, Style};


pub struct Stats {
    pub size: Rect,
    header: String,
    hourly: Vec<(String, u64)>,
    status: Vec<(String, u64)>,
    ips: Vec<(String, u64)>,
    reqs: Vec<(String, u64)>,
}

impl Stats {
    pub fn new(header: String, hourly: Vec<(String, u64)>, status: Vec<(String, u64)>, ips: Vec<(String, u64)>, reqs: Vec<(String, u64)>) -> Stats {
        Stats {
            size: Rect::default(),
            header: header,
            hourly: hourly,
            status: status,
            ips: ips,
            reqs: reqs
        }
    }
}

pub fn init_ui() -> Result<Terminal<RawBackend>, io::Error> {
    let backend = RawBackend::new()?;
    Terminal::new(backend)
}

fn draw_chart(t: &mut Terminal<RawBackend>, stats: &Stats, area: &Rect) {
    let vec_dates: Vec<(&str, u64)> = stats.hourly.iter().map(|(hour, count)| (&*hour as &str, *count)).collect();
    BarChart::default()
    .block(Block::default().title("Hour distribution").borders(Borders::ALL))
    .data(&vec_dates)
    .bar_width(6)
    .style(Style::default().fg(Color::Yellow))
    .value_style(Style::default().fg(Color::Black).bg(Color::Yellow))
    .render(t, area);
}

fn draw_lists(t: &mut Terminal<RawBackend>, stats: &Stats, area: &Rect) {
    Group::default()
        .direction(Direction::Horizontal)
        .sizes(&[Size::Percent(20), Size::Percent(35), Size::Percent(45)])
        .render(t, area, |t, chunks| {
            List::new(stats.status.iter().map(|(status, count)| {
                Item::Data(format!("{}: {}", status, count))
            })).block(Block::default().borders(Borders::ALL).title("Status codes distribution"))
            .render(t, &chunks[0]);
            List::new(stats.ips.iter().map(|(ips, count)| {
                Item::Data(format!("{}: {}", ips, count))
            })).block(Block::default().borders(Borders::ALL).title("Frequent IPs"))
            .render(t, &chunks[1]);
            List::new(stats.reqs.iter().map(|(reqs, count)| {
                Item::Data(format!("{}: {}", reqs, count))
            })).block(Block::default().borders(Borders::ALL).title("Frequent requests"))
            .render(t, &chunks[2]);
    });
}

fn draw_header(t: &mut Terminal<RawBackend>, stats: &Stats, area: &Rect) {
    Paragraph::default()
        .block(
            Block::default()
                .borders(Borders::ALL)
                .title("Statistics")
                .title_style(Style::default().fg(Color::Magenta)),
        )
        .wrap(true)
        .text(&stats.header)
        .render(t, area);
}

pub fn draw(t: &mut Terminal<RawBackend>, stats: &Stats) {
    Group::default()
        .direction(Direction::Vertical)
        .margin(2)
        .sizes(&[Size::Percent(15), Size::Percent(50), Size::Percent(35)])
        .render(t, &stats.size, |t, chunks| {
            draw_header(t, stats, &chunks[0]);
            draw_chart(t, stats, &chunks[1]);
            draw_lists(t, stats, &chunks[2]);
    });
    t.draw().unwrap();
}


pub fn stats_consumer(
    ips: BTreeMap<String, u64>,
    requests: BTreeMap<String, u64>,
    dates: BTreeMap<String, u64>,
    status_codes: BTreeMap<i64, u64>,
    filename: String,
    total: u64,
    failed: u64 ) -> Stats {

    let header = format!("File: {} \t Unique Ips: {} \t Unique requests: {}\nTotal Requests: {}\nFailed Requests: {}\n",
       filename, ips.len(), requests.len(), total, failed,
    );

    let mut sorted_dates: Vec<(String, u64)> = Vec::from_iter(dates);
    sorted_dates.sort_by(|&(_, a), &(_, b)| b.cmp(&a));

    let vec_status: Vec<(String, u64)> = status_codes.iter().map(|(status, count)| (status.to_string() , *count)).collect();

    let mut sorted_ips: Vec<(String, u64)> = Vec::from_iter(ips);
    sorted_ips.sort_by(|&(_, a), &(_, b)| b.cmp(&a));
    if sorted_ips.len() > 11 {
        sorted_ips = sorted_ips[0..11].to_vec()
    }

    let mut sorted_req: Vec<(String, u64)> = Vec::from_iter(requests);
    sorted_req.sort_by(|&(_, a), &(_, b)| b.cmp(&a));
    if sorted_req.len() > 11 {
        sorted_req = sorted_req[0..11].to_vec();
    }

    Stats::new(header, sorted_dates, vec_status, sorted_ips, sorted_req)
}

