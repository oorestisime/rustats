use std::io::{Error, ErrorKind};
use std::str::FromStr;
use chrono::NaiveDateTime;
use pom::Parser;
use pom::combinator::*;

#[derive(Debug, Clone)]
pub struct Log {
    pub ip_address: String,
    identd: String,
    username: String,
    pub time: NaiveDateTime,
    pub request: String,
    pub status_code: i64,
    size: Option<i64>,
    referrer: String,
    user_agent: String
}


pub fn parse_line(line: String) -> Result<Log, Error> {

    let parser = ipaddr() + untilspace() + untilspace() + space() * betweenbrackets() + space() * betweenquotes() + untilspace() + untilspace() + space() * betweenquotes() + space() * betweenquotes();
    let output = parser.parse(line.as_bytes());

    if let Ok(((((((((ip_address, identd), username), time), request), status_code), raw_size), referrer), user_agent)) = output {

        let size = match i64::from_str(&raw_size) {
            Ok(parse_size) => Some(parse_size),
            _ => None
        };

        return Ok(Log {
            ip_address: ip_address,
            identd: identd,
            username: username,
            time: NaiveDateTime::parse_from_str(&time, "%d/%b/%Y:%H:%M:%S %z").unwrap(),
            request: request,
            status_code: i64::from_str(&status_code).unwrap(),
            size: size,
            referrer: referrer,
            user_agent: user_agent,
        });
   }

    Err(Error::from(ErrorKind::InvalidData))
}

fn ipaddr<'a>() -> Combinator<impl Parser<'a, u8, Output=String>> {
    let string = one_of(b"1234567890.").repeat(0..);
    string.convert(String::from_utf8)
}

fn space<'a>() -> Combinator<impl Parser<'a, u8, Output=()>> {
    one_of(b" \t\r\n").repeat(0..).discard()
}

fn untilspace<'a>() -> Combinator<impl Parser<'a, u8, Output=String>> {

    let value = space() * none_of(b" ").repeat(0..);

    value.convert(String::from_utf8)
}

fn betweenbrackets<'a>() -> Combinator<impl Parser<'a, u8, Output=String>>{

    let value = sym(b'[') * none_of(b"]").repeat(0..) - sym(b']').discard();

    value.convert(String::from_utf8)
}

fn betweenquotes<'a>() -> Combinator<impl Parser<'a, u8, Output=String>> {
    let value = sym(b'"') * none_of(b"\"").repeat(0..) - sym(b'"').discard();

    value.convert(String::from_utf8)
}
