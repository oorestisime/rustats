#![feature(match_default_bindings)]
#![feature(conservative_impl_trait)]
extern crate chrono;
extern crate pom;
extern crate clap;
extern crate tui;
extern crate termion;

use termion::input::TermRead;

use std::io::{BufReader};
use std::io::prelude::*;
use std::fs::File;
use std::io;
use std::thread;
use std::collections::btree_map::BTreeMap;
use std::sync::mpsc;
use std::time;

use termion::event;
use chrono::Timelike;

use clap::{Arg, App};

pub mod draw;
pub mod parser;

enum Event {
    Input(event::Key),
    Tick,
}



fn main() {
    let matches = App::new("Rustats / CLF log analyzer")
        .version("0.0.1")
        .author("Orestis Ioannou <orestis@oioannou.com>")
        .about("Parses CLF logs and produces insights")
        .arg(Arg::with_name("filename")
            .help("Sets the file to use, - for stdin")
            .required(true)
            .index(1))
        .get_matches();
    let mut ips = BTreeMap::new();
    let mut status_codes = BTreeMap::new();
    let mut requests = BTreeMap::new();
    let mut dates: BTreeMap<String, u64> = BTreeMap::new();
    let mut total = 0;
    let mut failed = 0;
    {
        let stdin = io::stdin();
        let reader = match matches.value_of("filename").unwrap() {
            "-" => Box::new(stdin.lock()) as Box<BufRead>,
            path => Box::new(BufReader::new(File::open(&path).unwrap()))
        };
        for (i, line) in reader.lines().enumerate() {
            if let Ok(log) = parser::parse_line(line.unwrap()) {
                *ips.entry(log.ip_address).or_insert(0) += 1;
                *status_codes.entry(log.status_code).or_insert(0) += 1;
                *requests.entry(log.request).or_insert(0) += 1;
                *dates.entry(log.time.hour().to_string()).or_insert(0) += 1;
            } else {
                failed += 1;
            }
            total += 1;
            print!("\rParsed {} lines", i + 1);
        }
    }

    let mut drawer = draw::stats_consumer(ips, requests, dates, status_codes, matches.value_of("filename").unwrap().to_string(), total, failed);
    let mut terminal = draw::init_ui().expect("Failed initialization");
    terminal.clear().unwrap();
    terminal.hide_cursor().unwrap();
    drawer.size = terminal.size().unwrap();
    draw::draw(&mut terminal, &drawer);

    // Channels
    let (tx, rx) = mpsc::channel();
    let clock_tx = tx.clone();
    let input_tx = tx.clone();

     // Input
    thread::spawn(move || {
        let stdin = io::stdin();
        for c in stdin.lock().keys() {
            let evt = c.unwrap();
            input_tx.send(Event::Input(evt)).unwrap();
            if evt == event::Key::Char('q') {
                break;
            }
        }
    });

    thread::spawn(move || loop {
        clock_tx.send(Event::Tick).unwrap();
        thread::sleep(time::Duration::from_millis(500));
    });

    loop {
        let size = terminal.size().unwrap();
        if drawer.size != size {
            terminal.resize(size).unwrap();
            drawer.size = size;
        }

        let evt = rx.recv().unwrap();
        match evt {
            Event::Input(input) => if input == event::Key::Char('q') {
                break;
            },
            _ => ()
        }
        draw::draw(&mut terminal, &drawer);
    }

    terminal.show_cursor().unwrap();
}
